import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-cloud2',
  templateUrl: 'cloud2.html'
})
export class Cloud2Page {

  constructor(public navCtrl: NavController) {
  }
  
}
