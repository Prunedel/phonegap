import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-cart2',
  templateUrl: 'cart2.html'
})
export class Cart2Page {

  constructor(public navCtrl: NavController) {
  }
  
}
