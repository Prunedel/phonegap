import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Home2Page } from '../home2/home2';
import { Cart2Page } from '../cart2/cart2';
import { Cloud2Page } from '../cloud2/cloud2';

@Component({
  selector: 'page-menu2',
  templateUrl: 'menu2.html'
})
export class Menu2Page {

  constructor(public navCtrl: NavController) {
  }
  goToHome2(params){
    if (!params) params = {};
    this.navCtrl.push(Home2Page);
  }goToCart2(params){
    if (!params) params = {};
    this.navCtrl.push(Cart2Page);
  }goToCloud2(params){
    if (!params) params = {};
    this.navCtrl.push(Cloud2Page);
  }
}
